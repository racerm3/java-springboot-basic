# devfile-sample-java-springboot-basic
A basic sample application using Java Spring Boot with devfile

# docker
1. docker build -f docker/Dockerfile -t racerm3/java-springboot-basic .
1. docker push registry.gitlab.com/racerm3/java-springboot-basic
